# THX

TODO: Add a nice module description.

## Requirements
- `drupal/encrypt:^3.0`
- `drupal/real_aes:^2.3`

## Installation

In your project's composer.json:

```
"require": {
  ...
  "drupal/thx": "^1.0",
  ...
}
```

For the proper encryption of sensitive data is required to create `thx.key` in
your configured private directory. To create the binary file with a random
256-bit key use this command:

```bash
dd if=/dev/urandom bs=32 count=1 > /path/to/private-directory/thx.key
```

## Configuration

TODO: Explain the module configuration.