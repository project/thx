<?php

namespace Drupal\thx\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\thx\Service\ThxLibrary;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for THX.
 */
class ThxDeployAssetpool extends ConfigFormBase {

  /**
   * The THX library service.
   *
   * @var \Drupal\thx\Service\ThxLibrary
   */
  protected $thxLibrary;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * User data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * ThxDeployAssetpool constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\thx\Service\ThxLibrary $thxLibrary
   *   The THX Library service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Drupal\user\UserDataInterface $userData
   *   Userdata service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ThxLibrary $thxLibrary, Messenger $messenger, AccountProxyInterface $currentUser, UserDataInterface $userData) {
    $this->thxLibrary = $thxLibrary;
    $this->messenger = $messenger;
    $this->currentUser = $currentUser;
    $this->userData = $userData;

    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('config.factory'),
      $container->get('thx.thx_library'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'thx.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'thx_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('thx.settings');

    $form['blockchain_mandatory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make the blockchain registration mandatory'),
      '#default_value' => $config->get('blockchain_mandatory'),
    ];

    $form['blockchain_signup_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Signup users to the blockchain in the cron'),
      '#default_value' => $config->get('blockchain_signup_cron') ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="blockchain_mandatory"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['api_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API endpoint'),
      '#description' => $this->t('Enter the API url of your THX Blockchain'),
      '#maxlength' => 64,
      '#size' => 64,
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
      '#default_value' => $config->get('api_url') ?? 'https://api.thx.network',
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Enter you client id'),
      '#default_value' => $config->get('client_id'),
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('Enter you client secret'),
      '#default_value' => $config->get('client_secret'),
    ];

    $form['asset_pool'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Asset pool'),
      '#description' => $this->t('The current active asset pool'),
      '#default_value' => $config->get('asset_pool'),
    ];

    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
      '#default_value' => $config->get('access_token'),
    ];

    $form['access_token_expires'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token expiry'),
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
      '#default_value' => $config->get('access_token_expires') ? date('Y-m-d H:i:s', $config->get('access_token_expires')) : '',
    ];

    $form['health_info_btn'] = [
      '#type' => 'button',
      '#value' => $this->t('API health status'),
      '#ajax' => [
        'callback' => '::submitHealth',
      ],
    ];

    $form['token_info_btn'] = [
      '#type' => 'button',
      '#value' => $this->t('Renew access token'),
      '#ajax' => [
        'callback' => '::submitToken',
      ],
    ];

    $form['assetpool_info_btn'] = [
      '#type' => 'button',
      '#value' => $this->t('Assetpool information'),
      '#ajax' => [
        'callback' => '::submitAssetPoolInfo',
      ],
    ];

    $form['textarea'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => [
        'id' => 'api-status-info',
      ],
    ];

    if (empty($config->get('asset_pool'))) {
      $form['assetpool_info_btn']['#attributes']['disabled'] = 'disabled';
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax request for API health status.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   */
  public function submitHealth(array &$form, FormStateInterface $form_state): AjaxResponse {
    $ajax_response = new AjaxResponse();

    // Fetch test data from the sync service.
    $response = $this->thxLibrary->getHealth();
    $response = (string) Json::encode($response);
    $text = sprintf('<pre style="font-size: 10pt">%s</pre>', $response);

    $ajax_response->addCommand(new HtmlCommand('#api-status-info', $text));

    return $ajax_response;
  }

  /**
   * Ajax request for getting a new access token.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   */
  public function submitToken(array &$form, FormStateInterface $form_state): AjaxResponse {
    $ajax_response = new AjaxResponse();

    // Fetch test data from the sync service.
    $response = $this->thxLibrary->getToken();

    // Store the access token.
    if ($response !== FALSE) {

      // Store the access token and expiry.
      $this->config('thx.settings')
        ->set('access_token', $response['access_token'])
        ->set('access_token_expires', time() + (int) $response['expires_in'])
        ->save();
    }

    $response = (string) Json::encode($response);
    $text = sprintf('<pre style="font-size: 10pt">%s</pre>', $response);

    $ajax_response->addCommand(new HtmlCommand('#api-status-info', $text));

    return $ajax_response;
  }

  /**
   * Ajax request for current user info.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   */
  public function submitAssetPoolInfo(array &$form, FormStateInterface $form_state): AjaxResponse {
    $config = $this->config('thx.settings');

    $ajax_response = new AjaxResponse();

    // Fetch test data from the sync service.
    $response = $this->thxLibrary->getAssetPool($config->get('asset_pool'));
    $response = (string) Json::encode($response);
    $text = sprintf('<pre style="font-size: 10pt">%s</pre>', $response);

    $ajax_response->addCommand(new HtmlCommand('#api-status-info', $text));

    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('thx.settings')
      ->set('blockchain_mandatory', $form_state->getValue('blockchain_mandatory'))
      ->set('blockchain_signup_cron', $form_state->getValue('blockchain_signup_cron'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('asset_pool', $form_state->getValue('asset_pool'))
      ->save();
  }

}
