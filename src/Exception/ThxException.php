<?php

namespace Drupal\thx\Exception;

/**
 * Thx Exceptions.
 */
class ThxException extends \Exception {

  /**
   * Login failed exception.
   */
  public static function loginFailed(string $uid): ThxException {
    return new self(sprintf('Login failed for user with id: %s', $uid));
  }

  /**
   * Client login failed exception.
   */
  public static function clientLoginFailed(): ThxException {
    return new self('Login with client_credentials failed');
  }

}
