<?php

namespace Drupal\thx\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\encrypt\Exception\EncryptException;
use Drupal\thx\Exception\ThxException;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides a THX library of methods.
 */
class ThxLibrary {

  /**
   * Duplicate status code. Account already exists.
   *
   * @see https://dev.api.thx.network/v1/docs/#/Authentication/post_signup
   */
  const DUPLICATE = 422;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The asset pool.
   *
   * @var string
   */
  protected $assetPool;

  /**
   * The guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $guzzleClient;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The encryption service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface
   */
  protected $encryption;

  /**
   * The password generator.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected $passwordGenerator;

  /**
   * Is it run as the current user.
   *
   * @var bool
   */
  protected $runAsCurrent = FALSE;

  /**
   * ThxLibrary constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configfactory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The currentuser.
   * @param \Drupal\user\UserDataInterface $userData
   *   The userdata service.
   * @param \Drupal\encrypt\EncryptServiceInterface $encryption
   *   The encryption service.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $password_generator
   *   The password generator service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    UuidInterface $uuid,
    LoggerChannelFactoryInterface $logger,
    AccountProxyInterface $currentUser,
    UserDataInterface $userData,
    EncryptServiceInterface $encryption,
    PasswordGeneratorInterface $password_generator
  ) {
    $this->configFactory = $config_factory;
    $this->uuid = $uuid;
    $this->logger = $logger;
    $this->currentUser = $currentUser;
    $this->userData = $userData;
    $this->encryption = $encryption;
    $this->passwordGenerator = $password_generator;

    $config = $this->configFactory->getEditable('thx.settings');
    $this->assetPool = $config->get('asset_pool');

    $this->guzzleClient = new Client([
      'base_uri' => $config->get('api_url'),
      'allow_redirects' => FALSE,
      'timeout'         => 30,
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ]);
  }

  /**
   * Get an access token from client credentials.
   *
   * @return array|bool
   *   The response.
   */
  public function getToken() {
    $thxSettings = $this->configFactory->getEditable('thx.settings');

    try {
      $response = $this->guzzleClient->post('https://auth.thx.network/token', [
        'form_params' => [
          'client_id' => $thxSettings->get('client_id'),
          'client_secret' => $thxSettings->get('client_secret'),
          'grant_type' => 'client_credentials',
          'scope' => 'admin openid',
        ],
      ]);

    }
    catch (RequestException $e) {
      // Request failed.
      $this->logWrite('getToken failed: ' . $e->getMessage());
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Login call.
   *
   * @return string|false
   *   The response.
   *
   * @throws \Drupal\thx\Exception\ThxException
   */
  protected function login() {
    $accessToken = $this->configFactory->getEditable('thx.settings')->get('access_token');
    $expireDate = $this->configFactory->getEditable('thx.settings')->get('access_token_expires');

    // If the token is valid return that token.
    if (!empty($accessToken) && !empty($expireDate) && ($expireDate - time()) > 0) {
      return sprintf('Bearer %s', $accessToken);
    }

    // Try to refresh the token.
    $response = $this->getToken();

    if ($response === FALSE) {
      // No need to write to the log since getToken already did that.
      throw ThxException::clientLoginFailed();
    }

    // Store the access token and expiry.
    $this->configFactory->getEditable('thx.settings')
      ->set('access_token', $response['access_token'])
      ->set('access_token_expires', time() + (int) $response['expires_in'])
      ->save();

    return sprintf('Bearer %s', $response['access_token']);
  }

  /**
   * Signup call.
   *
   * @param int $accountId
   *   The user id.
   *
   * @return array|bool
   *   The response.
   *
   * @throws \Drupal\thx\Exception\ThxException
   * @throws \Drupal\encrypt\Exception\EncryptException
   */
  public function signup(int $accountId) {
    $account = User::load($accountId);

    if ($account === NULL) {
      $this->logWrite(sprintf('signup failed: User with id %s could not be found', $accountId));
      return FALSE;
    }

    $password = $this->passwordGenerator->generate(20);
    /** @var \Drupal\encrypt\Entity\EncryptionProfile $encryption_profile */
    $encryption_profile = EncryptionProfile::load('thx');

    if ($encryption_profile === NULL) {
      $this->logWrite(sprintf('signup failed: Encryption profile could not be found'));
      return FALSE;
    }
    // Encrypt the thx user password.
    try {
      $password_encrypted = $this->encryption->encrypt($password, $encryption_profile);
    }
    catch (EncryptException $e) {
      $this->logWrite('encryption failed: ' . $e->getMessage());
      return FALSE;
    }

    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->post('/v1/signup', [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
        'allow_redirects' => TRUE,
        'json' => [
          'email' => $account->getEmail(),
          'password' => $password,
          'confirmPassword' => $password,
        ],
      ]);
    }
    catch (RequestException $e) {
      if ($e->getCode() === static::DUPLICATE || strpos($e->getMessage(), 'An account with this e-mail address already exists')) {
        $this->userData->set('thx', $account->id(), 'account_exists', $account->getEmail());
      }
      $this->logWrite('signup failed: ' . $e->getMessage());
      return FALSE;
    }

    $content = Json::decode($response->getBody()->getContents());

    $this->userData->set('thx', $account->id(), 'username', $account->getEmail());
    $this->userData->set('thx', $account->id(), 'address', $content['address']);
    $this->userData->set('thx', $account->id(), 'password', $password_encrypted);

    return TRUE;
  }

  /**
   * Get the account of the logged in user.
   *
   * @return array|bool
   *   The response.
   */
  public function getAccount() {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get('/v1/account', [
        'headers' => [
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getAccount failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Get the membership status/information of a wallet_address in an assetpool.
   *
   * @param string $wallet_address
   *   The wallet address.
   *
   * @return array|bool
   *   The response.
   */
  public function getMembers(string $wallet_address) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get(sprintf('/v1/members/%s', $wallet_address), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getMembers failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Get information about a withdrawal.
   *
   * @param string $wallet_address
   *   The wallet address.
   *
   * @return array|bool
   *   The response.
   */
  public function getWithdrawal(string $wallet_address) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get(sprintf('/v1/withdrawals/%s', $wallet_address), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getWithdrawal failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Get a list of withdrawals for an address.
   *
   * @param string $wallet_address
   *   The wallet address.
   * @param bool $detailed
   *   Detailed or not.
   *
   * @return array|bool
   *   The response.
   */
  public function getWithdrawals(string $wallet_address, $detailed = FALSE) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get(sprintf('/v1/withdrawals?member=%s', $wallet_address), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getWithdrawals failed: %s', $e->getMessage()));
      return FALSE;
    }
    $withdrawPolls = Json::decode($response->getBody()->getContents());
    $data = [];
    foreach ($withdrawPolls['withdrawPolls'] as $withdrawall) {
      $data[] = $this->getWithdrawal($withdrawall);
    }

    return ($detailed === TRUE) ? $data : $withdrawPolls;
  }

  /**
   * Get health status of the API.
   *
   * @return array|bool
   *   The response.
   */
  public function getHealth() {
    try {
      $response = $this->guzzleClient->get('/v1/health');
    }
    catch (RequestException $e) {
      $this->logWrite(sprintf('getHealth failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Create reward.
   *
   * @param array $data
   *   Poll data.
   *
   * @return array|bool
   *   The response.
   */
  public function postPollFinalize(array $data) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->post(sprintf('/v1/rewards/%s/poll/finalize', $data['id']), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('postPollFinalize failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Get a single reward by id.
   *
   * @param string $reward_id
   *   The reward id.
   *
   * @return array|bool
   *   The response.
   */
  public function getReward(string $reward_id) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get(sprintf('/v1/rewards/%s', $reward_id), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getReward failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Create reward.
   *
   * @param array $data
   *   The reward data.
   *
   * @return array|bool
   *   The response.
   */
  public function postReward(array $data) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->post('/v1/rewards', [
        'allow_redirects' => TRUE,
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
        'json' => [
          'withdrawAmount' => $data['amount'],
          'withdrawDuration' => 400,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('postReward failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Give someone a reward.
   *
   * @param array $data
   *   The reward data.
   *
   * @return array|bool
   *   The response.
   */
  public function postGiveReward(array $data) {
    // Check if the reward exists.
    $checkReward = $this->getReward($data['id']);
    // Check if the user is member of the assetpool.
    $checkMember = $this->getMembers($data['member_address']);

    if ($checkReward === FALSE) {
      $this->logWrite(sprintf('postGiveReward failed: Reward %s not found', $data['id']));
      return FALSE;
    }

    // Check if the member.
    if ($checkMember === FALSE || (isset($checkMember['isMember']) && (bool) $checkMember['isMember'] !== TRUE)) {
      // If the user is not a member of the pool yet, add him.
      $memberResponse = $this->postMember($data['member_address']);

      if ($memberResponse === FALSE || (isset($memberResponse['isMember']) && $memberResponse['isMember'] === FALSE)) {
        $this->logWrite(sprintf('postGiveReward failed: user %s could not be added to asset pool', $data['member_address']));
        return FALSE;
      }
    }

    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->post(sprintf('/v1/rewards/%s/give', $data['id']), [
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
        'json' => [
          'member' => $data['member_address'],
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('postGiveReward failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Get an asset pool.
   *
   * @param string $assetPool
   *   The asset pool.
   *
   * @return array|bool
   *   The response.
   */
  public function getAssetPool(string $assetPool) {

    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->get(sprintf('/v1/asset_pools/%s', $assetPool), [
        'headers' => [
          'AssetPool' => $assetPool,
          'Authorization' => $authorization,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('getAssetPool failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Add member to asset pool.
   *
   * @param string $wallet_address
   *   The wallet address.
   *
   * @return array|bool
   *   The response.
   */
  public function postMember(string $wallet_address) {
    try {
      $authorization = $this->login();

      $response = $this->guzzleClient->post('/v1/members/', [
        'allow_redirects' => TRUE,
        'headers' => [
          'AssetPool' => $this->assetPool,
          'Authorization' => $authorization,
        ],
        'json' => [
          'address' => $wallet_address,
        ],
      ]);
    }
    catch (RequestException | ThxException $e) {
      $this->logWrite(sprintf('postMember failed: %s', $e->getMessage()));
      return FALSE;
    }

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Logger to log stuff.
   *
   * @param string $message
   *   The message to set.
   * @param int $level
   *   The log level.
   */
  protected function logWrite(string $message, $level = RfcLogLevel::CRITICAL): void {
    /*
     * $severity = RfcLogLevel::EMERGENCY;
     * $severity = RfcLogLevel::ALERT;
     * $severity = RfcLogLevel::CRITICAL;
     * $severity = RfcLogLevel::ERROR;
     * $severity = RfcLogLevel::WARNING;
     * $severity = RfcLogLevel::NOTICE;
     * $severity = RfcLogLevel::INFO;
     * $severity = RfcLogLevel::DEBUG;
     */
    $this->logger->get('thx')->log($level, $message);
  }

}
